<#
.SYNOPSIS
    Parse mp4/mkv metadata for all matching files in a directory using exiftool directory and outputs to a CSV.
.DESCRIPTION
    Recursively scans a target directory for .mp4 and .mkv files. Using the rensulting list, this script will 
    break the work into blocks of the specified size and process them with exiftool. By default it will process
    these blocks in parallel using the specified number of threads. Once all jobs are complete
    a single result csv will be assembled containing all of the parsed data.
.EXAMPLE
    PS C:\> .\Read-MediaMetaData.ps1 -Target "M:\tv-series"
    Parses all matching files in the target directory using the default block size and thread count.
    The results will be stored in csv format in a file named 'C:\Temp\metadata_M__tv-series__2021-02-19_2333.csv'.
.PARAMETER Target
    Directory which will be scanned for metadata
.PARAMETER LogDir
    Directory where the result csv will be stored
.PARAMETER BlockSize
    Number of files to process in each block. If set too high exiftool will fail.
.PARAMETER Threads
    Number of concurrently executing threads
.INPUTS
    None - you can't pipe to this script
.OUTPUTS
    None - only the direct CSV output is generated
.NOTES
    General notes
#>
param(
    [Parameter(Mandatory=$true)][String]  $Target,
    [Parameter(Mandatory=$false)][String] $LogDir    = "C:\Temp",
    [Parameter(Mandatory=$false)][Int32]  $BlockSize = 100,
    [Parameter(Mandatory=$false)][Int32]  $Threads   = 8
)

#=== Test-Function Arguments ==================================================
# Tests each parameter which can be validated. Also retrieves interactive
# parmaters like 'ad-credentials
function Test-Input {
    @($Target, $LogDir) | ForEach-Object {
        Write-Host "input: checking path '${_}'..." `
            -ForegroundColor Yellow -NoNewline
        if(Test-Path -LiteralPath $_ -PathType Container) {
            Write-Host "Success!" -ForegroundColor Green
        } else {
            Write-Host "Failed!" -ForegroundColor Red
            exit
        }
    }
}

#=== Test the Environment =====================================================
function Test-Environment {
    Write-Host "env: PowerShell Version..." `
        -ForegroundColor Yellow -NoNewLine
    $ps_major_version = $PSVersionTable.PSVersion.Major
    if ($ps_major_version -ge "7") {
        Write-Host "${ps_major_version} is supported!" `
            -ForegroundColor Green
    } else {
        Write-Host "${ps_major_version} is not supported!" `
            -ForegroundColor Red
            Write-Host "Please install 7.x or newer: https://github.com/powershell/powershell" `
            -ForegroundColor Red
        Exit
    }

    try {
        Write-Host "env: verifying exiftool is available..." `
            -ForegroundColor Yellow -NoNewline
        Get-Command 'exiftool' `
            -ErrorAction Stop | Out-Null
        Write-Host "Success!" -ForegroundColor Green
    } catch {
        Write-Host "Failed!" -ForegroundColor Red
        Write-Warning "exiftool.exe must be available in your path."
        Write-Warning "Perhaps install it using Chocolaty from https://chocolatey.org/packages/exiftool"
        exit
    }
}

#=== Splits the Input into equal chunks =======================================
function Get-Chunks {
    param(
        [Parameter(Mandatory=$true)][Int32] $BlockSize,
        [Parameter(Mandatory=$true)][Array] $Source
    )

    $chunks = [System.Collections.ArrayList]@()
    $idx = 0
    while($idx -lt $Source.Count) {
        $rangeEnd = if(($idx + $BlockSize - 1) -lt $Source.Count) {
            $idx + $blockSize - 1
        } else {
            $Source.Count - 1
        }
        $chunks.add($Source[$idx..$rangeEnd]) | Out-Null
        $idx = $idx + $BlockSize
    }

    # Return the chunked source
    $chunks
}

#=== Main Program Sequence ====================================================
Test-Environment
Test-Input

$timeStamp = Get-Date -Format yyyy-MM-dd_HHmm
$pathWithUnderscores = $Target -replace "[\\: &~]","_"
$logFilePrefix = "${LogDir}\metadata_${pathWithUnderscores}_${timeStamp}"

$stopWatch = [System.Diagnostics.StopWatch]::StartNew()

try {
    Push-Location $Target
} catch {
    Write-Host "Failed to change directory to '${Target}'!" `
        -ForegroundColor Red
    Write-Warning $_
    exit
}

# Pull a list of all of the files
$files = Get-ChildItem -Path . -Recurse -File | `
    Where-Object { $_.FullName -match '[.](mkv|mp4)$' } | `
    Select-Object -ExpandProperty FullName

# Break the range into chunks and process it 
$chunks = Get-Chunks -BlockSize $BlockSize -Source $files
$jobs = [System.Management.Automation.Job2[]]::new($chunks.Count)
for($cIdx = 0; $cIdx -lt $chunks.Count; $cIdx++) {
    $logFile = "${logFilePrefix}_${cIdx}.csv"
    $fileList = $chunks[$cIdx]
    $jobs[$cIdx] = Start-ThreadJob {
        exiftool.exe -ExtractEmbedded `
            -csv -progress $using:fileList | `
            Out-file $using:logFile
    } -ThrottleLimit $Threads
}

# Return to the source directory
Pop-Location

# Wait for the workers to finish
$completeJobs = 0
$JobCount = $jobs.Count

while ($completeJobs -lt $jobCount) {
    $timeElapsed = $stopWatch.Elapsed.TotalSeconds
    Write-Progress `
        -Activity "Processing $($files.Count) files" `
        -Status "${completeJobs}/${jobCount} jobs Complete in ${timeElapsed} seconds" `
        -PercentComplete (($completeJobs / $jobCount) * 100)
    $jobs | Wait-Job -Timeout 5 | Out-Null
    $jobStatus = Get-Job | Where-Object {
        ($_.State -NE "Running") -and ($_.State -ne "NotStarted")
    }

    $completeJobs = if ($null -eq $jobStatus) {
        0
    } elseif(($jobStatus.GetType().BaseType).Name -eq 'Array') {
        $jobStatus.Count
    } else {
        1
    }
}

$timeElapsed = $stopWatch.Elapsed.TotalSeconds
Write-Host "Processing completed in ${timeElapsed} seconds!"

$consolidatedLog = "${logFilePrefix}.csv"
for($f = 0; $f -lt $chunks.Count; $f++) {
    $logFile = "${logFilePrefix}_${f}.csv"
    Import-Csv -Path $logFile | `
        Export-csv -LiteralPath $consolidatedLog `
        -NoTypeInformation -Append -Force
    Remove-Item -LiteralPath $logFile -Force | Out-Null
}

# Prune anly lingering jobs
Get-Job | Remove-Job