## Pre-requisites

1. Install the following components

    |Component|Description|Notes|
    |---------|-----------|-----|
    |[PowerShell 7](https://github.com/powershell/powershell)|Current version of PowerShell Core|This script depends on [Start-ThreadJob](https://docs.microsoft.com/en-us/powershell/module/threadjob/start-threadjob) which is not present in PS5|
    |[Exiftool](https://exiftool.org/)|Metadata parser|Also `exiftool.exe` needs to be in the system path. You can install it with chocolaty.|

1. Make sure the target directory is mapped as a network drive as exiftool doesn't support UNC paths.
1. Before your first execution you may need to run the following command to allow unsigned scripts.

    ```powershell
    Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser
    ```

## Usage

Bascially, you just run it!

```powershell
# From the directory where you downloaded the script (in powershell 7)
.\Read-MediaMetaData.ps1 -Target "Z:\folder-with-my-stuff"
```

For additional details see the output of `Get-Help Read-MediaMetaData.ps1`.