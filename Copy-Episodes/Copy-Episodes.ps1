param (
    [Parameter(Mandatory=$true)][String] $FilePattern,
    [Parameter(Mandatory=$true)][String] $EpisodePattern,
    [Parameter(Mandatory=$true)][String] $SourcePath,
    [Parameter(Mandatory=$true)][String] $Season,
    [Parameter(Mandatory=$false)][Int32] $Offset = 0,
    [Parameter(Mandatory=$false)][ValidateSet('Copy','Move')][String] $mode = 'Copy',
    [Parameter(Mandatory=$false)][String] $TargetPath = '.',
    [Parameter(Mandatory=$false)][Bool] $WhatIf = $true
)

$entries = Get-ChildItem -LiteralPath $SourcePath | Where-Object { $_.Name -match $FilePattern }

$entries | Foreach-Object { Write-Verbose $_ }

if((Read-Host "Continue with copy of $($entries.Count) (y/n)") -ne 'y') {
    exit
}

foreach($e in $entries) {
    if ( $e.name -match $EpisodePattern ) {
        $episode = ([int]$Matches[1]) + $Offset
    } else {
        Continue
    }
    $filetype = if ($e.name -match '[.](\w+)$') {
        $Matches[1]
    } else {
        Write-Warning "'$($e.Name)}': Could not determine suffix... skipping!"
        continue
    }
    
    $arguments = @{
        LiteralPath = $e.FullName
        Destination = "${TargetPath}\S${Season}E${episode}.${filetype}"
        WhatIf      = $WhatIf
    }
    Write-Verbose "${Mode}: '$($arguments.LiteralPath)' -> '$($arguments.Destination)'"
    switch($Mode) {
        ('Copy') {
            Copy-Item @arguments
            break;
        }
        ('Move') {
            Move-Item @arguments
            break;
        }
    }
}
